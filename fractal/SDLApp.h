#pragma once

#include <SDL.h>

class SDLApp
{
private:
	bool Running;
	uint32_t ResX;
	uint32_t ResY;

	double ZoomTarget;
	double CameraXTarget;
	double CameraYTarget;

	double Zoom;
	double CameraX;
	double CameraY;

	double CameraSpeed;

	double ticker;

	SDL_Window* Window;
	SDL_Renderer* Renderer;
	SDL_Surface* WindowSurface;
	SDL_Surface* SoftwareSurface;

public:
	SDLApp();
	~SDLApp();

	int OnExecute();

private:
	bool OnInit();

	void OnEvent(SDL_Event &Event);

	void OnLoop();

	void OnRender();

	void OnCleanup();

	double DrawFunc(int x, int y);

	void PickColour(uint8_t* r, uint8_t* g, uint8_t* b, double value);

	void ScreenToMathCoord(int screenX, int screenY, double* mathX, double* mathY);
	void ScreenToMathCoordAlt(int screenX, int screenY, double* mathX, double* mathY);

	void AdjustCameraOnZoom(bool zoomAdjustment);

	void OnPrint(uint32_t currentFrame);
};

