#include "SDLApp.h"

#include <cmath>
#include <complex>
#include <iostream>
#include <string>
#include <windows.h>


using namespace std;


/*
Nice targets:
	CameraXTarget(0.03997689110000000000),
	CameraYTarget(-0.38012312700000000000),


	CameraXTarget(-0.12638369910000000000),
	CameraYTarget(-0.29447726300000000000),
*/
SDLApp::SDLApp() :
	Running(true),
	ResX(1000),
	ResY(1000),
	ZoomTarget(100),
	CameraXTarget(-0.12638369910000000000),
	CameraYTarget(-0.29447726300000000000),
	Zoom(ZoomTarget),
	CameraX(CameraXTarget),
	CameraY(CameraYTarget),
	CameraSpeed(10),
	ticker(0)
{

}


SDLApp::~SDLApp()
{
}

int SDLApp::OnExecute() {
	if (OnInit() == false) {
		return -1;
	}


	constexpr float FPS = 60;
	uint32_t waittime = (uint32_t) (1000.0f / FPS);
	uint32_t framestarttime = 0;
	int32_t delaytime;
	SDL_Event Event;
	uint32_t currentFrame = 0;
	uint32_t maxFrame = 1000000;
	maxFrame = 1000;
	while (currentFrame < maxFrame && Running) {
		while (SDL_PollEvent(&Event)) {
			OnEvent(Event);
		}

		OnLoop();
		OnRender();

		OnPrint(currentFrame);

		delaytime = waittime - (SDL_GetTicks() - framestarttime);
		if (delaytime > 0)
			SDL_Delay((uint32_t)delaytime);
		framestarttime = SDL_GetTicks();

		ZoomTarget *= 1.02;
		Zoom *= 1.02;
		if (currentFrame % 10 == 0) {
			SDL_Log("Current frame: %d out of %d", currentFrame + 1, maxFrame);
		}
		currentFrame++;
	}

	OnCleanup();

	return 0;
}


void SDLApp::OnPrint(uint32_t currentFrame) {
	// Create an empty RGB surface that will be used to create the screenshot bmp file
	//SDL_Surface* pScreenShot = SDL_CreateRGBSurface(0, ResX, ResY, 32, 0x00ff0000, 0x0000ff00, 0x000000ff, 0xff000000);

	// Read the pixels from the current render target and save them onto the surface
	//SDL_RenderReadPixels(Renderer, NULL, SDL_GetWindowPixelFormat(Window), pScreenShot->pixels, pScreenShot->pitch);

	// Create the bmp screenshot file
	char buffer[20];
	snprintf(buffer, 100, "output/nice%07d.bmp", currentFrame);
	if (SDL_SaveBMP(SoftwareSurface, buffer) == -1) {
		SDL_Log("Error");
	}

	// Destroy the screenshot surface
	//SDL_FreeSurface(pScreenShot);
}

bool SDLApp::OnInit() {
	if (SDL_Init(SDL_INIT_EVERYTHING) < 0) {
		return false;
	}

	Window = SDL_CreateWindow("The mandelbrot madness", SDL_WINDOWPOS_CENTERED,	SDL_WINDOWPOS_CENTERED,	ResX, ResY,	SDL_WINDOW_RESIZABLE | SDL_WINDOW_OPENGL);

	if (Window == NULL) {
		return false;
	}
	WindowSurface = SDL_GetWindowSurface(Window);
	//Renderer = SDL_CreateRenderer(Window, -1, SDL_RENDERER_ACCELERATED);
	SoftwareSurface = SDL_CreateRGBSurface(0, ResX, ResY, 32, 0x00ff0000, 0x0000ff00, 0x000000ff, 0xff000000);
	Renderer = SDL_CreateSoftwareRenderer(SoftwareSurface);
	
	return true;
}

void SDLApp::OnEvent(SDL_Event &Event) {
	if (Event.type == SDL_QUIT) {
		Running = false;
	}
	else if(Event.type == SDL_KEYDOWN && Event.key.repeat == 0) {
		switch( Event.key.keysym.sym ) { 
			case SDLK_KP_PLUS:
				CameraSpeed *= 4;
				break;
			case SDLK_KP_MINUS:
				CameraSpeed *= 0.25;
				CameraSpeed = max(CameraSpeed, 0.01);
				break;
			case SDLK_SPACE:
				int x, y;
				double mathX, mathY;
				SDL_GetMouseState(&x, &y);
				ScreenToMathCoordAlt(x, y, &mathX, &mathY);
				SDL_Log("X: %20.20f, Y: %20.20f\n", mathX, mathY);
			default: 
				break; 
		} 
	}
	else if (Event.type == SDL_WINDOWEVENT && Event.window.event == SDL_WINDOWEVENT_RESIZED) {
		ResX = Event.window.data1;
		ResY = Event.window.data2;
	}
	else if (Event.type == SDL_MOUSEWHEEL) {
		double multiplier = 1;
		const Uint8* keyboardState = SDL_GetKeyboardState(NULL);
		if (keyboardState[SDL_SCANCODE_LSHIFT]) {
			multiplier = 4;
		}
		double zoomAdjustment;
		bool wheelUp = Event.wheel.y > 0;
		if (wheelUp) {
			zoomAdjustment = multiplier * 4;
		}
		else {
			zoomAdjustment = 0.25 / multiplier;
		}
		AdjustCameraOnZoom(wheelUp);
		ZoomTarget *= zoomAdjustment;
	}
}

void SDLApp::AdjustCameraOnZoom(bool zoomIn) {
	double cameraMoveModifier;
	if (zoomIn) {
		cameraMoveModifier = 1;
	}
	else {
		cameraMoveModifier = -1;
	}
	int x, y, xDiff, yDiff;
	double xDiffMath, yDiffMath;
	SDL_GetMouseState(&x, &y);
	xDiff = x - ResX / 2;
	yDiff = ResY / 2 - y;
	xDiffMath = xDiff / Zoom;
	yDiffMath = yDiff / Zoom;
	CameraXTarget += xDiffMath * cameraMoveModifier;
	CameraYTarget += yDiffMath * cameraMoveModifier;
}

void SDLApp::OnLoop() {
	double camdiff = CameraXTarget - CameraX;
	CameraX += camdiff * 0.8;
	camdiff = CameraYTarget - CameraY;
	CameraY += camdiff * 0.8;
	camdiff = ZoomTarget - Zoom;
	Zoom += camdiff * 0.8;

	double multiplier = 1;
	const Uint8* keyboardState = SDL_GetKeyboardState(NULL);
	if (keyboardState[SDL_SCANCODE_LSHIFT]) {
		multiplier = 10;
	}
	if (keyboardState[SDL_SCANCODE_UP]) {
		CameraYTarget += CameraSpeed * multiplier / Zoom;
	}
	if (keyboardState[SDL_SCANCODE_DOWN]) {
		CameraYTarget -= CameraSpeed * multiplier / Zoom;
	}
	if (keyboardState[SDL_SCANCODE_LEFT]) {
		CameraXTarget -= CameraSpeed * multiplier / Zoom;
	}
	if (keyboardState[SDL_SCANCODE_RIGHT]) {
		CameraXTarget += CameraSpeed * multiplier / Zoom;
	}

	ticker -= 4;
}

void SDLApp::OnRender() {
	uint8_t r, g, b, a;
	a = 0xFF;

	SDL_SetRenderDrawColor(Renderer, 0x55, 0x55, 0x55, 0xFF);
	SDL_RenderClear(Renderer);
	for (uint32_t i = 0; i < ResX; i++) { 
		for (uint32_t j = 0; j < ResY; j++) {
			double val = DrawFunc(i, j);
			PickColour(&r, &b, &g, val);
			SDL_SetRenderDrawColor(Renderer, r, g, b, 0xFF);
			SDL_RenderDrawPoint(Renderer, i, j);
		}
	}
	SDL_RenderPresent(Renderer);
}

void SDLApp::OnCleanup() {
	SDL_Quit();
}

double SDLApp::DrawFunc(int screenX, int screenY) {
	double x, y;
	ScreenToMathCoord(screenX, screenY, &x, &y);

	complex<double> init(0, 0);
	complex<double> prev(init);
	complex<double> modded;
	for (int i = 0; i < 100; ++i) {
		modded = ((prev + complex<double>(x, y)) * prev + complex<double>(x, y) * prev) + complex<double>(x, y);
		if (abs(modded) > 2) {
			return abs(modded);
		}
		prev = modded;
	}
	return -abs(modded);
}

void SDLApp::PickColour(uint8_t* r, uint8_t* g, uint8_t* b, double value) {
	if (value <= 0) {
		value += ticker / 100;
		*r = (value + value)*(value + 1);
		*g = value * value * 100;
		*b = value * value * value * 100;
	}
	else {
		value += ticker / 256;
		*r = (value+value)*(value+1) + ticker;
		*g = value*value*100 + ticker;
		*b = value*100 + ticker;
	}
	/*
	if (value <= 0) {
		//value += ticker;
		*r = 2;
		*g = 165;
		*b = 222;
	}
	else {
		//value += ticker;
		*r = 222;
		*g = 111;
		*b = 222;
	}
	*/

}

void SDLApp::ScreenToMathCoord(int screenX, int screenY, double* mathX, double* mathY) {
	int midX = ResX / 2;
	int midY = ResY / 2;
	int diffX = screenX - midX;
	int diffY = midY - screenY;
	double x = diffX / Zoom;
	double y = diffY / Zoom;
	*mathX = x + CameraX;
	*mathY = y + CameraY;
}

void SDLApp::ScreenToMathCoordAlt(int screenX, int screenY, double* mathX, double* mathY) {
	int midX = ResX / 2;
	int midY = ResY / 2;
	int diffX = screenX - midX;
	int diffY = midY - screenY;
	double x = diffX / Zoom;
	double y = diffY / Zoom;
	*mathX = x + CameraX;
	*mathY = y + CameraY;
}


